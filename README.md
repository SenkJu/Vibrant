# Vibrant
 A Minecraft Hacked Client written in Kotlin and AspectJ

## License
This project is subject to the [GNU General Public License v3.0](LICENSE). This does only apply for source code located directly in this clean repository. Since the compilation requires the use of [MCP](http://www.modcoderpack.com/) in order to properly compile a working Minecraft client application, it will generate additional source code. This additional code is not covered by the license nor do we hold any rights to it.

For those who are not familiar with the license I will summarize the main points. This is by no means legal advise nor legally binding.

You are allowed to
- use
- share
- modify

this project entirely or partially for free and even commercially. However, please consider the following:

- **You must disclose the source code of your modified work or the source code you took from this project. This means you are not allowed to use code from this project (even partially) in a closed-source (or even obfuscated) application.**
- **Your modified application must also be licensed under the GPL** 

Do the above and share your source code with everyone; just like we do.

## Compilation
1. Clone `https://gitlab.com/Cydhra/Vibrant.git`. 
2. If you are using IntelliJ, import the project as a gradle project (otherwise IntelliJ might reject the gradle model). While importing,
you should uncheck "create separate module per source set" and check "use auto-import". The first setting is mandatory for the start configs to 
work. Sadly, we cannot enforce those settings using the repository, because the file that saves these settings contains platform-specific
paths.
3. Reset everything that your IDE deletes and changes.
4. Run the `setupDecompWorkspace` gradle task for the module you want to build (currently only 1.8.8).
5. Reload the gradle project from the gradle tool window in IntelliJ.
6. On linux: We use the library [JCEF](https://bitbucket.org/chromiumembedded/java-cef) which is being a little sissy about some files. 
Currently there is only a workaround available: Create symbolic links to the following files inside your `lib/cef-natives` folder (note, that
you must have completed step 4 for the files being present): `natives_blob.bin`, `snapshot_blob.bin` and `icudtl.dat`. These symbolic links must 
reside in your default jre installation `bin` folder (the default installation refers to the installation used by IntelliJ).
6. After completing all the steps, you should be able to run the project from within IntelliJ using the shipped starting configuration. Building the
client as an archive to run it from the launcher is currently untested and may not work. However it is supposed to work using the `shadowJar`
 gradle task

Warning: `build` does not work work on MacOS. The library [JCEF](https://bitbucket.org/chromiumembedded/java-cef) does not release binaries
and since nobody built them manually for us, yet, we do not have access to them. Feel free to contribute them to 
[JCEF-platform](https://gitlab.com/Cydhra/jcef-platform)

## Contributing

We always appreciate contributions to this project. If you want to get started, have a look at the modules. Those are isolated for the most 
part, therefore easier to understand and to get into. If you do not like Kotlin and Java, feel free to check the GUI implementation and 
help with it: It is written in HTML, CSS and JavaScript. 