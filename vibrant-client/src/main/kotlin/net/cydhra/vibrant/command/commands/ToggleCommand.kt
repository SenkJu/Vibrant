package net.cydhra.vibrant.command.commands

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import net.cydhra.vibrant.modulesystem.ModuleManager
import org.cef.callback.CefQueryCallback

/**
 * Command to toggle a module
 */
object ToggleCommand : CommandHandler<ToggleArguments>(userCommand = true) {

    override fun executeCommand(origin: CommandSender, label: String, arguments: ToggleArguments, callback: CefQueryCallback?) {
        val moduleList = ModuleManager.modules
                .filter { arguments.modules.map { it.toLowerCase().replace("-", "") }.contains(it.name.toLowerCase().replace("-", "")) }

        moduleList.forEach { module ->
            if (arguments.state == Mode.ENABLE) {
                module.isEnabled = true
            } else if (arguments.state == Mode.DISABLE) {
                module.isEnabled = false
            } else {
                module.toggle()
            }
        }

        if (!moduleList.isEmpty())
            callback?.success("Toggled ${moduleList.joinToString(", ") { it.name }}")
        else
            callback?.failure(-1, "No module found.")
    }
}

class ToggleArguments(parser: ArgParser) {
    val state by parser.mapping("--enable" to Mode.ENABLE,
            "--disable" to Mode.DISABLE,
            "--toggle" to Mode.TOGGLE,
            help = "select to which state the modules shall be changed"
    ).default(Mode.TOGGLE)

    val modules by parser.positionalList("MODULES", "the modules that shall be toggled")
}

enum class Mode {
    ENABLE, DISABLE, TOGGLE
}