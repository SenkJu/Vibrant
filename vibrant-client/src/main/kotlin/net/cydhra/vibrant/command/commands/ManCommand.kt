package net.cydhra.vibrant.command.commands

import com.xenomachina.argparser.ArgParser
import net.cydhra.vibrant.command.CommandDispatcher
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import org.cef.callback.CefQueryCallback

/**
 * Prints the man page of another command, if present
 */
object ManCommand : CommandHandler<ManArguments>(userCommand = true) {
    override fun executeCommand(origin: CommandSender, label: String, arguments: ManArguments, callback: CefQueryCallback?) {
        val manText = this.javaClass.getResourceAsStream("/man/${arguments.page}.html")?.reader()?.readText()

        if (manText == null) {
            callback?.failure(-1, "There is no man page for ${arguments.page}.")
            return
        }

        callback?.success("<div class=\"manpage\">$manText</div>")
    }
}

class ManArguments(parser: ArgParser) {
    val page by parser.positional("PAGE", "the requested man page")
}