package net.cydhra.vibrant.command.commands

import com.xenomachina.argparser.ArgParser
import net.cydhra.vibrant.VibrantClient
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import org.cef.callback.CefQueryCallback

object RenameWorldCommand : CommandHandler<RenameWorldArguments>(userCommand = false) {
    override fun executeCommand(origin: CommandSender, label: String, arguments: RenameWorldArguments, callback: CefQueryCallback?) {
        try {
            VibrantClient.minecraft.getSaveLoaderInstance().rename(arguments.world, arguments.newName)
        } catch (e: Exception) {
            callback?.failure(-1, e.message)
            return
        }

        callback?.success("Renamed ${arguments.world} to ${arguments.newName}")
    }

}

class RenameWorldArguments(parser: ArgParser) {
    val world by parser.positional("WORLD", help = "name of the world folder whose world's name to change")
    val newName by parser.positional("NAME", help = "new name for the world")
}