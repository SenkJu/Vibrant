package net.cydhra.vibrant.command

import org.cef.callback.CefQueryCallback
import java.net.URL

@FunctionalInterface
abstract class CommandHandler<T>(val userCommand: Boolean, val proxyCommand: Boolean = false) {
    abstract fun executeCommand(origin: CommandSender, label: String, arguments: T, callback: CefQueryCallback?)
}