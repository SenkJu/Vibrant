package net.cydhra.vibrant.configuration

import com.fasterxml.jackson.databind.module.SimpleModule
import com.uchuhimo.konf.Config
import com.uchuhimo.konf.ConfigSpec
import com.uchuhimo.konf.source.json.toJson
import net.cydhra.vibrant.configuration.mapper.ColorDeserializer
import net.cydhra.vibrant.configuration.mapper.ColorSerializer
import java.awt.Color
import java.io.File

/**
 * Provides the configuration related services. Register configuration specs and sources here.
 */
object ConfigurationService {

    const val SETTINGS_FOLDER_NAME = "vibrant"
    val settingsFolder = File(SETTINGS_FOLDER_NAME).apply { mkdir() }
    val settingsFile = File(settingsFolder, "configuration.json")

    private val configurationSources: MutableSet<ConfigurationSource> = mutableSetOf()
    private val configurationSpecs: MutableSet<ConfigSpec> = mutableSetOf()

    var config: Config = Config()
        private set

    fun init() {
        this.registerSource(ConfigurationFileSource(ConfigurationService.settingsFile.apply { takeIf { it.createNewFile() }
                ?.also { it.writeText("{}") } }))
    }

    /**
     * Add a new source to the configuration. It overrides all previously configured sources. In order to set the new source active, call
     * [reloadConfig]
     *
     * @param source an implementation of [ConfigurationSource] that provides the raw source
     */
    fun registerSource(source: ConfigurationSource) {
        configurationSources += source
    }

    /**
     * Remove a previously registered source from the configuration. For the change to be effective, call [reloadConfig].
     *
     * @param source a previously registered source
     */
    fun unregisterSource(source: ConfigurationSource) {
        configurationSources -= source
    }

    /**
     * Add a specification to the configuration. To take effect in the model, reload the config with [reloadConfig]
     *
     * @param spec the [ConfigSpec] object
     */
    fun addSpecification(spec: ConfigSpec) {
        this.configurationSpecs += spec
    }

    /**
     * Reload the configuration with the current sources
     */
    fun reloadConfig() {
        this.config = Config {
            this@ConfigurationService.configurationSpecs.forEach(this::addSpec)
        }

        config.mapper.registerModule(SimpleModule().apply {
            addSerializer(Color::class.java, ColorSerializer())
            addDeserializer(Color::class.java, ColorDeserializer())
        })

        this.configurationSources.forEach { this.config = it.applySource(this.config) }
    }

    /**
     * Save the current configuration. The configuration must have been loaded before at least one time
     */
    fun saveConfig() {
        this.config.toJson.toFile(this.settingsFile.apply { createNewFile() })
    }

}