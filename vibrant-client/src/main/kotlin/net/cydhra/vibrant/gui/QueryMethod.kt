package net.cydhra.vibrant.gui

annotation class QueryMethod(val name: String, val userCommand: Boolean = false)