package net.cydhra.vibrant.gui.modules

import net.cydhra.eventsystem.listeners.EventHandler
import net.cydhra.vibrant.events.minecraft.KeyboardActionEvent
import net.cydhra.vibrant.gui.clickgui.ClickGuiGuiManager
import net.cydhra.vibrant.modulesystem.DefaultCategories
import net.cydhra.vibrant.modulesystem.Module
import org.lwjgl.input.Keyboard

class ClickGuiModule : Module("ClickGUI", DefaultCategories.SYSTEM) {

    override fun initialize() {
        this.isEnabled = true
    }

    @EventHandler
    fun onKeyPressed(e: KeyboardActionEvent) {
        if (e.type == KeyboardActionEvent.KeyboardAction.RELEASE && !mc.isCurrentlyDisplayingScreen) {
            // TODO make configurable
            if (e.keycode == Keyboard.KEY_RSHIFT) {
                mc.displayGuiScreen(ClickGuiGuiManager.createGuiScreen())
            }
        }
    }
}