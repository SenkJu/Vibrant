package net.cydhra.vibrant.gui.modules

import net.cydhra.eventsystem.listeners.EventHandler
import net.cydhra.vibrant.VibrantClient
import net.cydhra.vibrant.command.CommandDispatcher
import net.cydhra.vibrant.command.CommandSender
import net.cydhra.vibrant.events.minecraft.ChatEvent
import net.cydhra.vibrant.events.minecraft.KeyboardActionEvent
import net.cydhra.vibrant.gui.terminal.TerminalGuiManager
import net.cydhra.vibrant.modulesystem.DefaultCategories
import net.cydhra.vibrant.modulesystem.Module
import org.cef.OS
import org.cef.callback.CefQueryCallback
import org.lwjgl.input.Keyboard

class TerminalModule : Module("Terminal", DefaultCategories.SYSTEM) {

    override fun initialize() {
        this.isEnabled = true
    }

    @EventHandler
    fun onKeyPressed(e: KeyboardActionEvent) {
        // on linux, keyboard support is vastly broken. do not use the gui there, use chat commands
        if (!OS.isLinux()) {
            if (e.type == KeyboardActionEvent.KeyboardAction.RELEASE && !mc.isCurrentlyDisplayingScreen) {
                // TODO make configurable
                if (e.keycode == Keyboard.KEY_Y) {
                    mc.displayGuiScreen(TerminalGuiManager.createGuiScreen())
                }
            }
        }
    }

    @EventHandler
    fun onChatEvent(e: ChatEvent) {
        if (e.message.startsWith(".")) { // TODO make configurable
            e.isCancelled = true
            CommandDispatcher.dispatchCommand(e.message.removePrefix("."), CommandSender.PLAYER, object : CefQueryCallback {
                override fun success(msg: String) {
                    VibrantClient.minecraft.thePlayer!!.displayChatMessageOnClient(factory.newChatTextMessage(msg))
                }

                override fun failure(errorCode: Int, msg: String) {
                    System.err.println("Error while command dispatching ($errorCode): $msg")
                    VibrantClient.minecraft.thePlayer!!.displayChatMessageOnClient(factory.newChatTextMessage("[ERROR] $msg"))
                }
            })
        }
    }
}