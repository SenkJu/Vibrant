package net.cydhra.vibrant.modules.movement

import net.cydhra.eventsystem.listeners.EventHandler
import net.cydhra.vibrant.api.network.VibrantPlayerPacket
import net.cydhra.vibrant.events.network.PacketEvent
import net.cydhra.vibrant.modulesystem.DefaultCategories
import net.cydhra.vibrant.modulesystem.Module
import org.lwjgl.input.Keyboard

/**
 * A module with the purpose of preventing fall damage being applied to the player.
 */
class NoFallModule : Module("NoFall", DefaultCategories.MOVEMENT, Keyboard.KEY_N) {

    @EventHandler
    fun onPacket(e: PacketEvent) {
        if (e.packet is VibrantPlayerPacket) {
            if (mc.thePlayer!!.fallDistance > 2.5) {
                (e.packet as VibrantPlayerPacket).onGround = true
                mc.thePlayer!!.fallDistance = 0f
            }
        }
    }
}