package net.cydhra.vibrant.modules.movement

import net.cydhra.eventsystem.listeners.EventHandler
import net.cydhra.vibrant.events.minecraft.MinecraftTickEvent
import net.cydhra.vibrant.modulesystem.DefaultCategories
import net.cydhra.vibrant.modulesystem.Module
import org.lwjgl.input.Keyboard

/**
 * A module that forces sprinting to be active.
 */
class SprintModule : Module("Sprint", DefaultCategories.MOVEMENT, Keyboard.KEY_G) {

    @EventHandler
    fun onTick(e: MinecraftTickEvent) {
        if (mc.thePlayer?.isCollidedHorizontally == false && mc.thePlayer!!.movementInput.moveForward > 0 && mc.thePlayer!!.onGround) {
            mc.thePlayer!!.isEntitySprinting = true
        }
    }
}