package net.cydhra.vibrant.api.network.server

interface VibrantPacketWindowProperty {
    val windowIdentifier: Int
    val variableIndex: Int
    val value: Int
}