package net.cydhra.vibrant.api.world.blocks

import net.cydhra.vibrant.api.world.VibrantBlockInfo

interface VibrantBlockStairs : VibrantBlockInfo
interface VibrantBlockFalling : VibrantBlockInfo