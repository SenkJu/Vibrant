package net.cydhra.vibrant.api.inventory

import net.cydhra.vibrant.api.item.VibrantItemStack


interface VibrantPlayerInventory : VibrantInventory {

    var currentSelectedIndex: Int
    val currentSelectedItemStack: VibrantItemStack?

    fun getArmorInventory(): Array<VibrantItemStack>
}