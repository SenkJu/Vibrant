package net.cydhra.vibrant.api.world.storage

interface VibrantSaveFormatComparator {
    val fileName: String
    val displayName: String
}