class DOMBuilder {

    constructor() {
        this.moduleManager = new ModuleManager();
        /*         this.vibrant = new Vibrant(); */
    }

    createPanel(panelName) {
        this.panel = document.createElement("div");
        this.panel.classList.add("panel");

        let title = document.createElement("div");
        title.classList.add("title");
        title.innerHTML = panelName;
        this.panel.appendChild(title);

        let modules = document.createElement("div");
        modules.classList.add("modules");
        this.panel.appendChild(modules);

        this.makeDraggable(this.panel);

        return this;
    }

    addModule(moduleName, isEnabled) {
        this.moduleName = moduleName;

        let moduleContainer = document.createElement("div");
        moduleContainer.classList.add("module");
        moduleContainer.innerHTML = this.moduleName;

        if (isEnabled) {
            moduleContainer.classList.add("enabled");
        }

        this.panel.getElementsByClassName("modules")[0].appendChild(moduleContainer);

        let moduleC = new Module(this.moduleName, moduleContainer);

        moduleContainer.addEventListener("click", () => {
            moduleC.toggle();
        });

        moduleContainer.addEventListener("contextmenu", (e) => {
            e.preventDefault();

            moduleC.toggleSettings();
        });

        this.moduleManager.registerModule(moduleC);

        return this;
    }

    addSettings(settings) {
        let settingsContainer = document.createElement("div");
        settingsContainer.classList.add("settings");

        let moduleName = this.moduleName;

        for (let settingName in settings) {

            switch (settings[settingName].type.toLowerCase()) {
                case "range":
                    let rangeContainer = document.createElement("div");
                    rangeContainer.classList.add("setting", "range");

                    let rangeDescription = document.createElement("span");
                    rangeDescription.innerHTML = settingName;
                    rangeContainer.appendChild(rangeDescription);

                    let range = document.createElement("input");
                    range.setAttribute("type", "range");
                    range.setAttribute("min", settings[settingName].min);
                    range.setAttribute("max", settings[settingName].max);
                    range.setAttribute("value", settings[settingName].value);
                    rangeContainer.appendChild(range);

                    settingsContainer.appendChild(rangeContainer);

                    range.addEventListener("change", () => {
                        Vibrant.setValue(moduleName, settingName, range.value);
                    });

                    break;


                case "boolean":
                    let booleanContainer = document.createElement("div");
                    booleanContainer.classList.add("setting", "boolean");

                    let booleanLabel = document.createElement("label");
                    booleanLabel.classList.add("checkbox-container");
                    booleanContainer.appendChild(booleanLabel);

                    let booleanDescription = document.createElement("span");
                    booleanDescription.innerHTML = settingName;
                    booleanLabel.appendChild(booleanDescription);

                    let booleanInput = document.createElement("input");
                    booleanInput.setAttribute("type", "checkbox");

                    if (settings[settingName].value) {
                        booleanInput.setAttribute("checked", "true");
                    }

                    booleanLabel.appendChild(booleanInput);

                    let booleanCheckmark = document.createElement("span");
                    booleanCheckmark.classList.add("checkmark");
                    booleanLabel.appendChild(booleanCheckmark);

                    settingsContainer.appendChild(booleanContainer);

                    booleanInput.addEventListener("change", () => {
                        Vibrant.setValue(moduleName, settingName, booleanInput.checked);
                    });

                    break;

                case "enum":
                    let enumContainer = document.createElement("div");
                    enumContainer.classList.add("setting", "enum");

                    let enumDescription = document.createElement("span");
                    enumDescription.innerHTML = settingName;
                    enumContainer.appendChild(enumDescription);

                    let select = document.createElement("select");

                    for (let value of settings[settingName].values) {
                        let option = document.createElement("option");
                        option.innerHTML = value;
                        option.setAttribute("value", value);
                        select.appendChild(option);
                    }

                    enumContainer.appendChild(select);

                    settingsContainer.appendChild(enumContainer);

                    select.addEventListener("change", () => {
                        Vibrant.setValue(moduleName, settingName, select.value);
                    });


                    break;
            }
        }

        this.moduleManager.getModule(moduleName).addSettings(settingsContainer);

        this.panel.getElementsByClassName("modules")[0].appendChild(settingsContainer);

        return this;
    }

    finalize() {
        let clickGuiContainer = document.getElementById("clickgui");

        clickGuiContainer.appendChild(this.panel);
    }

    makeDraggable(el) {
        let pos1 = 0;
        let pos2 = 0;
        let pos3 = 0;
        let pos4 = 0;

        el.addEventListener("mousedown", getInForeground);

        let dragTitle = el.getElementsByClassName("title")[0];

        dragTitle.onmousedown = dragMouseDown;

        function dragMouseDown(e) {
            e = e || window.event;
            e.preventDefault();

            pos3 = e.clientX;
            pos4 = e.clientY;
            document.onmouseup = closeDragElement;

            document.onmousemove = elementDrag;
        }

        function elementDrag(e) {
            e = e || window.event;
            e.preventDefault();

            pos1 = pos3 - e.clientX;
            pos2 = pos4 - e.clientY;
            pos3 = e.clientX;
            pos4 = e.clientY;
            el.style.top = (el.offsetTop - pos2) + "px";
            el.style.left = (el.offsetLeft - pos1) + "px";
        }

        function closeDragElement() {
            document.onmouseup = null;
            document.onmousemove = null;
        }

        function getInForeground() {
            let panels = document.getElementsByClassName("panel");

            let highestZIndex = 0;
            for (let panel of panels) {
                let zIndex = parseInt(calcStyle(panel).zIndex);

                if (zIndex > highestZIndex) {
                    highestZIndex = zIndex;
                }
            }

            el.style.zIndex = (highestZIndex + 1).toString();
        }

        function calcStyle(el) {
            return el.currentStyle || window.getComputedStyle(el);
        }
    }
}