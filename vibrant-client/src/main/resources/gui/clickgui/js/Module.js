class Module {

    constructor(moduleName, moduleDom) {

        this.moduleName = moduleName;
        this.moduleDom = moduleDom;
        this.state = false;
    }

    addSettings(settingsDom) {
        this.settingsDom = settingsDom;
    }

    toggle() {
        let classList = this.moduleDom.classList;

        (classList.contains("enabled")) ? classList.remove("enabled") : classList.add("enabled"); 

        Vibrant.toggleModule(this.moduleName, classList.contains("enabled"));
    }

    toggleSettings() {
        if (this.settingsDom) {
            let classList = this.settingsDom.classList;

            (classList.contains("shown")) ? classList.remove("shown") : classList.add("shown"); 
        }
    }

    isEnabled() {
        return this.state;
    }
}