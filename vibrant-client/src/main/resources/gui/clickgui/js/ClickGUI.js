class ClickGUI {

    constructor() {
        this.moduleManager = new ModuleManager();
        this.domBuilder = new DOMBuilder();
    }

    init(modulesList, toggledList) {
        for (let category in modulesList) {
            let modules = modulesList[category];

            let panel = this.domBuilder.createPanel(category);

            for (let moduleName of modules.sort()) {

                console.log(JSON.stringify(toggledList));
                // modulename, toggled?
                panel.addModule(moduleName, toggledList[moduleName]);

                let moduleSettings = {
                    Range: {
                        type: "range",
                        min: 0,
                        max: 10,
                        value: 3
                    },
                    CPS: {
                        type: "range",
                        min: 0,
                        max: 10,
                        value: 3
                    },
                    Velocity: {
                        type: "boolean",
                        value: true
                    },
                    Velocity: {
                        type: "boolean",
                        value: false
                    },
                    Axolotl: {
                        type: "boolean",
                        value: true
                    },
                    AttackMode: {
                        type: "enum",
                        values: ["Multi", "Single", "Switch"],
                        value: "Multi"
                    }
                }

                panel.addSettings(moduleSettings);
            }

            panel.finalize();
        }
    }
}