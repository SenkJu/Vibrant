class Vibrant {

    static setValue(moduleName, settingName, value) {
        console.log(moduleName, settingName, value);
    }

    static toggleModule(moduleName, state) {

        //TODO: Return success
        window.vibrantQuery({
            "request": "toggle " + ((state) ? "--enable" : "--disable") + " " + moduleName,
            "onSuccess": function (response) {
            },
            "onFailure": function (code, response) {
            }
        });
    }
}