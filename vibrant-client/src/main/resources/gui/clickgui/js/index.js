const clickgui = new ClickGUI();

window.vibrantQuery({
    "request": "modules",
    "onSuccess": function (modules) {

        window.vibrantQuery({
            "request": "get manager.modules.enabledModules",
            "onSuccess": function (toggledModules) {
                clickgui.init(JSON.parse(modules), JSON.parse(toggledModules));
            },
            "onFailure": function (code, response) {
                console.log(response);
            }
        });
    },
    "onFailure": function (code, response) {
        console.log(response);
    }
});