class ModuleManager {

    constructor() {
        this.registeredModules = {};
    }

    registerModule(moduleObj) {
        this.registeredModules[moduleObj.moduleName] = moduleObj;
    }

    getModule(moduleName) {
        return this.registeredModules[moduleName];
    }
}