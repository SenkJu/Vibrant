/**
 * Returns the respective DOM object for a given identifier.
 *
 * @param {String} el - DOM element referred to, eg. by ID or class name
 */

function _(el) {
    return document.querySelector(el);
}

/**
 * Removes a given DOM object from the document body.
 *
 * @param {Object} el - The DOM object to be deleted
 */

function removeElement(el) {
    el.parentElement.removeChild(el);
}

/**
 * Logs in to a Minecraft account.
 *
 * @param {String} username - Username of account
 * @param {String} email - E-Mail of account
 * @param {String} password - Password of account
 * @param {Boolean} isOnline - Whether or not the Mojang servers should be contacted when logging in
 * @param {Object} targetAlt - DOM object of the target alt.
 */
function vibrantLogin(username, email, password, isOnline, targetAlt) {

    var command = `login -m ${email} -u ${username} -p ${password}`;
    if (!isOnline) command += " -o";

    window.vibrantQuery({
        "request": command,
        "onSuccess": function (response) {
            var jsonResponse = JSON.parse(response);

            window.vibrantQuery({
               "request": `session -a --autorefresh ${response}`,
               "onSuccess": function(response) {
                    console.log("success");
                },
                "onFailure": function (code, response) {
                    console.log("error");
                }
            });
            altCount.innerHTML = "Logged in as " + jsonResponse.alias;

            targetAlt.classList.add("loggedin");
            targetAlt.setAttribute("data-alias", jsonResponse.alias);
            targetAlt.getElementsByClassName("username")[0].innerHTML = jsonResponse.alias;

            // Sets avatar to alias name
            var headImg = targetAlt.getElementsByTagName("img")[0];
            headImg.src = "https://minotar.net/avatar/" + jsonResponse.alias;

            // Reset background color to default
            setTimeout(() => {
                targetAlt.classList.remove("loggedin");
            }, 1000);
        },
        "onFailure": function (code, response) {
            altCount.innerHTML = "Failed login.";

            targetAlt.classList.add("failed");

            // Reset background color to default
            setTimeout(() => {
                targetAlt.classList.remove("failed");
            }, 1000);
        }
    });
}

/**
 * Adds an alt to the alt list.
 *
 * @param {String} username - Username of account
 * @param {String} email - E-Mail of account
 * @param {String} password - Password of account
 * @param {Boolean} isOnline - Whether or not the Mojang servers should be contacted when logging in
 * @param {String} alias - Alias name for the alt
 */
function addAlt(username, email, password, isOnline, alias) {
    var alt = document.createElement("div");
    alt.classList.add("alt");

    // Add alt information to the alt element
    alt.setAttribute("data-username", username);
    alt.setAttribute("data-email", email);
    alt.setAttribute("data-password", password);
    if (alias) alt.setAttribute("data-alias", alias);

    var head = document.createElement("div");
    head.classList.add("head");
    alt.appendChild(head);

    var headImg = document.createElement("img");

    // Set preview image to either username or alias
    if (alias && alias !== "null") {
        headImg.src = "https://minotar.net/avatar/" + alias;
    } else {
        headImg.src = "https://minotar.net/avatar/" + username;
    }

    head.appendChild(headImg);

    var usernameEl = document.createElement("div");
    usernameEl.classList.add("info", "username");

    // Set username feild to either email address or username or alias
    (username && username !== "null") ? usernameEl.innerHTML = username : usernameEl.innerHTML = email;
    if (alias) usernameEl.innerHTML = alias;

    alt.appendChild(usernameEl);

    var type = document.createElement("div");
    type.classList.add("info", "type");
    type.innerHTML = (isOnline) ? "Premium" : "Cracked";
    alt.appendChild(type);

    var buttons = document.createElement("div");
    buttons.classList.add("buttons");
    alt.appendChild(buttons);

    var loginButton = document.createElement("button");
    loginButton.classList.add("login");
    buttons.appendChild(loginButton);

    var loginButtonInner = document.createElement("i");
    loginButtonInner.classList.add("fa", "fa-check");
    loginButtonInner.setAttribute("aria-hidden", "true");
    loginButton.appendChild(loginButtonInner);

    var deleteButton = document.createElement("button");
    deleteButton.classList.add("delete");
    buttons.appendChild(deleteButton);

    var loginDeleteButton = document.createElement("i");
    loginDeleteButton.classList.add("fa", "fa-times");
    loginDeleteButton.setAttribute("aria-hidden", "true");
    deleteButton.appendChild(loginDeleteButton);

    // Bind Delete button
    deleteButton.addEventListener("click", () => {
        removeAlt(alt);
    });

    // Bind Login button
    loginButton.addEventListener("click", (event) => {
        var targetAlt = event.target.parentElement.parentElement.parentElement;
        vibrantLogin(username, email, password, isOnline, targetAlt, targetAlt);
    });

    altContainer.appendChild(alt);

    setAltAmount();
    saveAlts();
}

/**
 * Saves all alts to the local storage
 */
function saveAlts() {
    var altArray = [];

    var altElements = document.getElementsByClassName("alt");

    // Saves each alt independently
    for (var i = 0; i < altElements.length; i++) {
        altArray[i] = {
            username: altElements[i].getAttribute("data-username"),
            password: altElements[i].getAttribute("data-password"),
            email: altElements[i].getAttribute("data-email"),
            alias: altElements[i].getAttribute("data-alias")
        };
    }

    localStorage.setItem("alts", JSON.stringify(altArray));
}

/**
 * Load all alts from the local storage
 */
function loadAlts() {
    if (localStorage.getItem("alts")) {
        var altArray = JSON.parse(localStorage.getItem("alts"));

        for (var i = 0; i < altArray.length; i++) {
            var email = altArray[i].email;
            var username = altArray[i].username;
            var password = altArray[i].password;
            var alias = altArray[i].alias;
            var isPremium = (password && password !== "null") ? true : false;

            addAlt(username, email, password, isPremium, alias);
        }
    }
}

/**
 * Removes an alt from the AltManager list.
 *
 * @param {Object} alt - DOM element of the alt to be removed
 */
function removeAlt(alt) {
    removeElement(alt);
    setAltAmount();
    saveAlts();
}

/**
 * Clears alt input fields.
 */
function clearInputs() {
    var inputs = document.getElementsByTagName("input");

    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].type === "password" || inputs[i].type === "text") {
            inputs[i].value = "";
        }
    }
}

/**
 * Sets the current amount of alts.
 */
function setAltAmount() {
    var alts = document.getElementsByClassName("alt");

    altCount.innerHTML = alts.length + " Alts(s)";
}

/**
 * Returns all information needed for logging in to an account.
 *
 * @param {String} usernameEmail - Username/E-Mail of an account
 * @param {String} password - Password of an account
 * @returns {Object} - Object containing username, password, E-Mail and and offline/online value of an account.
 */
function getCredentials(usernameEmail, password) {
    // Did user enter an E-Mail address or a username?
    if (usernameEmail.includes("@")) {
        var email = usernameEmail;
        var username = null;
    } else {
        var email = null;
        var username = usernameEmail;
    }

    // Did user provide a password? -> online : offline
    if (password) {
        var isOnline = true;
        var password = password;
    } else {
        var isOnline = false;
        var password = null;
    }

    var response = {
        username: username,
        password: password,
        email: email,
        isOnline: isOnline
    }

    return response;
}

const inputEmailPassword = _("#inputEmailPassword");
const buttonAdd = _("#buttonAdd");
const buttonBack = _("#buttonBack");
const buttonReload = _("#buttonReload");
const inputEmail = _("#inputEmail");
const inputPassword = _("#inputPassword");

const altContainer = _("#altContainer");
const altCount = _("#altCount");

buttonAdd.addEventListener("click", () => {
    if (inputEmailPassword.value) {
        var split = inputEmailPassword.value.split(":");

        var credentials = getCredentials(split[0], split[1]);
    } else {
        var credentials = getCredentials(inputEmail.value, inputPassword.value);
    }

    addAlt(credentials.username, credentials.email, credentials.password, credentials.isOnline, null);

    clearInputs();
});

buttonBack.addEventListener("click", () => {
    window.location = "MainMenu.html";
});

buttonReload.addEventListener("click", () => {
    window.vibrantQuery({ "request": "reloadgui" });
});

window.addEventListener("load", () => {
    loadAlts();
});

window.addEventListener("beforeunload", () => {
    saveAlts();
});
