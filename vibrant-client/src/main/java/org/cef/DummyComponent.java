package org.cef;

import java.awt.*;

/**
 * A dummy component for AWT that is used in {@link org.cef.browser.CefBrowserOsrLwjgl} to comply to the interface. The component is not
 * actually required, since the browser is not used in an AWT environment.
 */
public class DummyComponent extends Component {

    @Override
    public Point getLocationOnScreen() {
        return new Point(0, 0);
    }

}
