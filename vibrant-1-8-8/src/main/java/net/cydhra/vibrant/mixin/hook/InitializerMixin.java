package net.cydhra.vibrant.mixin.hook;

import net.cydhra.vibrant.VibrantClient;
import net.cydhra.vibrant.adapter.VibrantFactoryImpl;
import net.cydhra.vibrant.api.client.VibrantMinecraft;
import net.minecraft.client.Minecraft;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(Minecraft.class)
public abstract class InitializerMixin {

    @Inject(method = "startGame", at = @At("RETURN"))
    protected void onStartGame(final CallbackInfo info) {
        VibrantClient.INSTANCE.init((VibrantMinecraft) Minecraft.getMinecraft(), new VibrantFactoryImpl());
    }
}
