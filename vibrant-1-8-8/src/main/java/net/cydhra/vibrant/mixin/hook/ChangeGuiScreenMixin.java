package net.cydhra.vibrant.mixin.hook;

import net.cydhra.eventsystem.EventManager;
import net.cydhra.vibrant.api.gui.VibrantGuiController;
import net.cydhra.vibrant.events.minecraft.GuiScreenChangeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@SuppressWarnings("UnusedAssignment")
@Mixin(Minecraft.class)
public abstract class ChangeGuiScreenMixin {

    @Inject(method = "displayGuiScreen", at = @At("HEAD"), cancellable = true)
    protected void onDisplayGuiScreen(GuiScreen screen, final CallbackInfo info) {
        GuiScreenChangeEvent event = new GuiScreenChangeEvent((VibrantGuiController) screen);
        EventManager.callEvent(event);

        if (event.isCancelled())
            info.cancel();
        else
            screen = (GuiScreen) event.getScreen();
    }
}
