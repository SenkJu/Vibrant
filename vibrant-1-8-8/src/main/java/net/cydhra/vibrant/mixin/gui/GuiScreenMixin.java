package net.cydhra.vibrant.mixin.gui;

import net.cydhra.vibrant.api.gui.VibrantGuiController;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(GuiScreen.class)
public abstract class GuiScreenMixin implements VibrantGuiController {

    @Shadow
    protected abstract void actionPerformed(GuiButton button);

    @Override
    public void drawRectWithCustomSizedTexture(int x, int y, float u, float v, int width, int height, float textureWidth, float textureHeight) {
        Gui.drawModalRectWithCustomSizedTexture(x, y, u, v, width, height, textureWidth, textureHeight);
    }

    @Override
    public void actionPerformed(int id) {
        this.actionPerformed(new GuiButton(id, 0, 0, ""));
    }
}
