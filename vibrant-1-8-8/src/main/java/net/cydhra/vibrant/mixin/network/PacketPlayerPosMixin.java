package net.cydhra.vibrant.mixin.network;

import net.cydhra.vibrant.api.network.VibrantPlayerPosPacket;
import net.minecraft.network.play.client.C03PacketPlayer;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(C03PacketPlayer.C04PacketPlayerPosition.class)
public abstract class PacketPlayerPosMixin extends C03PacketPlayer implements VibrantPlayerPosPacket {

    @Override
    public void setPosX(double posX) { this.x = posX; }

    @Override
    public void setPosY(double posY) { this.y = posY; }

    @Override
    public void setPosZ(double posZ) { this.z = posZ; }

    @Override
    public double getPosX() { return x; }

    @Override
    public double getPosY() { return y; }

    @Override
    public double getPosZ() { return z; }
}
