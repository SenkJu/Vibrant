package net.cydhra.vibrant.mixin.network;

import net.cydhra.vibrant.api.network.VibrantPlayerLookPacket;
import net.minecraft.network.play.client.C03PacketPlayer;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(C03PacketPlayer.C05PacketPlayerLook.class)
public abstract class PacketPlayerLookMixin extends C03PacketPlayer implements VibrantPlayerLookPacket {

    @Override
    public void setYaw(float yaw) { this.yaw = yaw; }

    @Override
    public void setPitch(float pitch) { this.pitch = pitch; }

    @Override
    public float getYaw() { return this.yaw; }

    @Override
    public float getPitch() { return this.pitch; }
}
