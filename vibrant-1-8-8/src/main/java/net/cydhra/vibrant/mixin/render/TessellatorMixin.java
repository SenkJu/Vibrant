package net.cydhra.vibrant.mixin.render;

import net.cydhra.vibrant.api.render.VibrantTessellator;
import net.minecraft.client.renderer.Tessellator;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(Tessellator.class)
public abstract class TessellatorMixin implements VibrantTessellator {
}
