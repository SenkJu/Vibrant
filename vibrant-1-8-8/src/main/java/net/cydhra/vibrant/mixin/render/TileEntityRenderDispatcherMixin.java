package net.cydhra.vibrant.mixin.render;

import net.cydhra.vibrant.api.render.VibrantTileEntityRendererDispatcher;
import net.cydhra.vibrant.api.tileentity.VibrantTileEntity;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.tileentity.TileEntity;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(TileEntityRendererDispatcher.class)
public abstract class TileEntityRenderDispatcherMixin implements VibrantTileEntityRendererDispatcher {

    @Shadow
    public abstract void renderTileEntity(TileEntity entity, float partialTicks, int destroyStage);

    @Override
    public void doRenderTileEntity(@NotNull VibrantTileEntity tileEntity, float partialTicks, int destroyStage) {
        this.renderTileEntity((TileEntity) tileEntity, partialTicks, destroyStage);
    }
}
