package net.cydhra.vibrant.mixin.world.blocks;

import net.cydhra.vibrant.api.world.blocks.VibrantBlockFalling;
import net.minecraft.block.BlockFalling;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(BlockFalling.class)
public abstract class BlockFallingMixin implements VibrantBlockFalling {
}
