package net.cydhra.vibrant.mixin.item;

import net.cydhra.vibrant.api.item.VibrantItemBook;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBook;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(ItemBook.class)
public abstract class ItemBookMixin extends Item implements VibrantItemBook {

}
