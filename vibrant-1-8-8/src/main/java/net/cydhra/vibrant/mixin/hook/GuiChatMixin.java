package net.cydhra.vibrant.mixin.hook;

import net.cydhra.eventsystem.EventManager;
import net.cydhra.vibrant.events.minecraft.ChatEvent;
import net.minecraft.client.gui.GuiScreen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(GuiScreen.class)
public class GuiChatMixin {

    @Inject(method = "sendChatMessage(Ljava/lang/String;Z)V", at = @At("HEAD"), cancellable = true)
    public void onSendChatMessage(String message, boolean addToHistory, CallbackInfo info) {
        ChatEvent event;
        EventManager.callEvent(event = new ChatEvent(message));
        if (event.isCancelled()) {
            info.cancel();
        }
    }
}
