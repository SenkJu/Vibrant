package net.cydhra.vibrant.mixin.hook;

import net.cydhra.eventsystem.EventManager;
import net.cydhra.vibrant.api.network.VibrantPacket;
import net.cydhra.vibrant.events.network.PacketEvent;
import net.minecraft.network.INetHandler;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketThreadUtil;
import net.minecraft.network.ThreadQuickExitException;
import net.minecraft.util.IThreadListener;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(PacketThreadUtil.class)
public class PacketReceiveMixin {

    @Overwrite
    public static <T extends INetHandler> void checkThreadAndEnqueue(final Packet<T> packet, final T netHandler, IThreadListener listener) throws
            ThreadQuickExitException {
        if (!listener.isCallingFromMinecraftThread()) {
            EventManager.callEvent(new PacketEvent(PacketEvent.EventType.RECEIVE, (VibrantPacket) packet));
            listener.addScheduledTask(() -> packet.processPacket(netHandler));
            throw ThreadQuickExitException.field_179886_a;
        }
    }

}
