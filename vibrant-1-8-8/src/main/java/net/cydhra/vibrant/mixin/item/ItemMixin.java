package net.cydhra.vibrant.mixin.item;

import net.cydhra.vibrant.api.item.VibrantItem;
import net.minecraft.item.Item;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(Item.class)
public abstract class ItemMixin implements VibrantItem {

    @Shadow
    public abstract int getItemEnchantability();

    @Override
    public int getEnchantability() {
        return this.getItemEnchantability();
    }
}
