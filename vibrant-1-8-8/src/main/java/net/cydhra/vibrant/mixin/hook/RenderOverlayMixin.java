package net.cydhra.vibrant.mixin.hook;

import net.cydhra.eventsystem.EventManager;
import net.cydhra.vibrant.api.render.VibrantScaledResolution;
import net.cydhra.vibrant.events.render.RenderOverlayEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngame;
import net.minecraft.client.gui.ScaledResolution;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(GuiIngame.class)
public abstract class RenderOverlayMixin {

    @Inject(method = "renderGameOverlay", at = @At("HEAD"))
    protected void onRenderGameOverlay(float partialTicks, final CallbackInfo info) {
        EventManager.callEvent(new RenderOverlayEvent(
                (VibrantScaledResolution) new ScaledResolution(Minecraft.getMinecraft()), partialTicks));
    }
}
