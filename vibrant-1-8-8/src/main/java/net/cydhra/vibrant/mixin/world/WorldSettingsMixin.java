package net.cydhra.vibrant.mixin.world;

import net.cydhra.vibrant.api.world.VibrantWorldSettings;
import net.minecraft.world.WorldSettings;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(WorldSettings.class)
public abstract class WorldSettingsMixin implements VibrantWorldSettings {
}
