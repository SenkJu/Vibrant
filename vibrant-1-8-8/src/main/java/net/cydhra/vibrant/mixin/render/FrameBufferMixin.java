package net.cydhra.vibrant.mixin.render;

import net.cydhra.vibrant.api.render.VibrantFramebuffer;
import net.minecraft.client.shader.Framebuffer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(Framebuffer.class)
public abstract class FrameBufferMixin implements VibrantFramebuffer {

    @Shadow
    public int framebufferTextureWidth;

    @Shadow
    public int framebufferTextureHeight;

    @Shadow
    public int framebufferWidth;

    @Shadow
    public int framebufferHeight;

    @Shadow
    public int framebufferTexture;

    @Shadow
    public int depthBuffer;

    @Override
    public int getDepthBuffer() {
        return this.depthBuffer;
    }

    @Override
    public void setDepthBuffer(int depthBuffer) {
        this.depthBuffer = depthBuffer;
    }

    @Override
    public int getWidth() {
        return this.framebufferWidth;
    }

    @Override
    public int getHeight() {
        return this.framebufferHeight;
    }

    @Override
    public int getTextureWidth() {
        return this.framebufferTextureWidth;
    }

    @Override
    public int getTextureHeight() {
        return this.framebufferTextureHeight;
    }

    @Override
    public int getTextureId() {
        return this.framebufferTexture;
    }
}
