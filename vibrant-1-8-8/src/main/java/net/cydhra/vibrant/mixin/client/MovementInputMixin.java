package net.cydhra.vibrant.mixin.client;

import net.cydhra.vibrant.api.client.VibrantMovementInput;
import net.minecraft.util.MovementInput;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(MovementInput.class)
public abstract class MovementInputMixin implements VibrantMovementInput {

    @Shadow
    public float moveStrafe;

    @Shadow
    public float moveForward;

    @Shadow
    public boolean jump;

    @Shadow
    public boolean sneak;

    @Override
    public float getMoveStrafe() {
        return this.moveStrafe;
    }

    @Override
    public void setMoveStrafe(final float moveStrafe) {
        this.moveStrafe = moveStrafe;
    }

    @Override
    public float getMoveForward() {
        return this.moveForward;
    }

    @Override
    public void setMoveForward(final float moveForward) {
        this.moveForward = moveForward;
    }

    @Override
    public boolean getJump() {
        return this.jump;
    }

    @Override
    public void setJump(final boolean jump) {
        this.jump = jump;
    }

    @Override
    public boolean getSneak() {
        return this.sneak;
    }

    @Override
    public void setSneak(final boolean sneak) {
        this.sneak = sneak;
    }
}
