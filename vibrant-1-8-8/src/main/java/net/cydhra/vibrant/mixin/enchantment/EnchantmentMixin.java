package net.cydhra.vibrant.mixin.enchantment;

import net.cydhra.vibrant.api.enchantment.VibrantEnchantment;
import net.cydhra.vibrant.api.item.VibrantItem;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnumEnchantmentType;
import net.minecraft.item.Item;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(Enchantment.class)
public abstract class EnchantmentMixin implements VibrantEnchantment {

    @Shadow
    public int effectId;

    @Shadow
    public EnumEnchantmentType type;

    @Shadow
    public abstract int getMinLevel();

    @Shadow
    public abstract int getMaxLevel();

    @Shadow
    public abstract int getMinEnchantability(int level);

    @Shadow
    public abstract int getMaxEnchantability(int level);

    @Shadow
    public abstract boolean canApplyTogether(Enchantment other);

    @Shadow
    public abstract String getTranslatedName(int level);

    @Shadow
    private int weight;

    @Override
    public int getEffectIdentifier() {
        return effectId;
    }

    @Override
    public boolean canEnchantItem(@NotNull VibrantItem item) {
        return this.type.canEnchantItem((Item) item);
    }

    @Override
    public int getMinimumLevel() {
        return this.getMinLevel();
    }

    @Override
    public int getMaximumLevel() {
        return this.getMaxLevel();
    }

    @Override
    public int getMinEnchantmentThreshold(int enchantmentLevel) {
        return this.getMinEnchantability(enchantmentLevel);
    }

    @Override
    public int getMaxEnchantmentThreshold(int enchantmentLevel) {
        return this.getMaxEnchantability(enchantmentLevel);
    }

    @Override
    public boolean isCompatible(@NotNull VibrantEnchantment other) {
        return this.canApplyTogether((Enchantment) other);
    }

    @NotNull
    @Override
    public String getDisplayName(int level) {
        return this.getTranslatedName(level);
    }

    @Override
    public int getWeight() {
        return this.weight;
    }
}
