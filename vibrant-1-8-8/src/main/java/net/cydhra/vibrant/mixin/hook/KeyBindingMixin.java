package net.cydhra.vibrant.mixin.hook;

import net.cydhra.eventsystem.EventManager;
import net.cydhra.vibrant.events.minecraft.KeyboardActionEvent;
import net.minecraft.client.settings.KeyBinding;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@SuppressWarnings("UnusedAssignment")
@Mixin(KeyBinding.class)
public abstract class KeyBindingMixin {

    @Inject(method = "setKeyBindState", at = @At("HEAD"), cancellable = true)
    private static void onSetKeyBindState(int keycode, boolean pressed, final CallbackInfo info) {
        if (keycode != 0) {
            final KeyboardActionEvent event;
            //@formatter:off
            EventManager.callEvent(event = new KeyboardActionEvent(pressed ? KeyboardActionEvent.KeyboardAction.PRESS : KeyboardActionEvent.KeyboardAction.RELEASE, keycode));
            //@formatter:on
            if (event.isCancelled()) { info.cancel(); } else {
                keycode = event.getKeycode();
                pressed = event.getType() == KeyboardActionEvent.KeyboardAction.PRESS.ordinal();
            }
        }
    }
}
