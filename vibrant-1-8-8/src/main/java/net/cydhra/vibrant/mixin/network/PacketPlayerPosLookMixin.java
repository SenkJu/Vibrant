package net.cydhra.vibrant.mixin.network;

import net.cydhra.vibrant.api.network.VibrantPlayerPosLookPacket;
import net.minecraft.network.play.client.C03PacketPlayer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(C03PacketPlayer.C06PacketPlayerPosLook.class)
public abstract class PacketPlayerPosLookMixin extends C03PacketPlayer implements VibrantPlayerPosLookPacket {

    public void setPosX(double posX) { this.x = posX; }

    public void setPosY(double posY) { this.y = posY; }

    public void setPosZ(double posZ) { this.z = posZ; }

    public double getPosX() { return x; }

    public double getPosY() { return y; }

    public double getPosZ() { return z; }

    public void setYaw(float yaw) { this.yaw = yaw; }

    public void setPitch(float pitch) { this.pitch = pitch; }

    public float getYaw() { return this.yaw; }

    public float getPitch() { return this.pitch; }
}
