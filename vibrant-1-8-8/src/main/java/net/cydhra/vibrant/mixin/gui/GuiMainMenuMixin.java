package net.cydhra.vibrant.mixin.gui;

import net.cydhra.vibrant.api.gui.VibrantGuiMainMenu;
import net.minecraft.client.gui.GuiMainMenu;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(GuiMainMenu.class)
public abstract class GuiMainMenuMixin implements VibrantGuiMainMenu {

}
