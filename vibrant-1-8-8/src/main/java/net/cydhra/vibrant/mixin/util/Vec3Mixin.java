package net.cydhra.vibrant.mixin.util;

import net.cydhra.vibrant.api.util.VibrantVec3;
import net.minecraft.util.Vec3;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(Vec3.class)
public abstract class Vec3Mixin implements VibrantVec3 {

    @Shadow
    public double xCoord;

    @Shadow
    public double yCoord;

    @Shadow
    public double zCoord;

    @Override
    public double getXCoord() { return this.xCoord; }

    @Override
    public double getYCoord() { return this.yCoord; }

    @Override
    public double getZCoord() { return this.zCoord; }
}
