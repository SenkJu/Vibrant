package net.cydhra.vibrant.mixin.hook;

import net.cydhra.eventsystem.EventManager;
import net.cydhra.vibrant.api.network.VibrantPacket;
import net.cydhra.vibrant.events.network.PacketEvent;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.Packet;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@SuppressWarnings("UnusedAssignment")
@Mixin(NetHandlerPlayClient.class)
public abstract class PacketMixin {

    @Inject(method = "addToSendQueue", at = @At("HEAD"), cancellable = true)
    protected void onAddToSendQueue(Packet packet, final CallbackInfo info) {
        PacketEvent packetEvent = new PacketEvent(PacketEvent.EventType.SEND, (VibrantPacket) packet);
        EventManager.callEvent(packetEvent);

        if (packetEvent.isCancelled()) {
            info.cancel();
        } else {
            packet = (Packet) packetEvent.getPacket();
        }
    }
}
