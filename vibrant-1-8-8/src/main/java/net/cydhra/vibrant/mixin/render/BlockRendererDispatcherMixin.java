package net.cydhra.vibrant.mixin.render;

import net.cydhra.vibrant.api.render.VibrantBlockRendererDispatcher;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(BlockRendererDispatcher.class)
public abstract class BlockRendererDispatcherMixin implements VibrantBlockRendererDispatcher {
}
